#!/bin/bash
# This code is licensed under CC0 1.0 Universal: https://creativecommons.org/publicdomain/zero/1.0/legalcode
# Author: Andre Klapper

PHABURL="https://phabricator.wikimedia.org"
CONDUITTOKEN="api-xxxxxxxxxxxxxxxxxxxxxxxxxxxx"

echo "* URL of Phabricator installation:" $PHABURL
echo "* Your Conduit Token (see $PHABURL/settings/panel/apitokens/ ):" $CONDUITTOKEN
echo "* Note: The local path for arc is hardcoded in this script to /var/www/html/phab/arcanist/bin/arc"
read -p "* Enter the Phabricator username of the user whose actions should be reverted (without @ at the beginning): " USERNAME
echo "* Username set to:" $USERNAME

# Get ID for username:
USERID=( $(echo '{"constraints":{"usernames":["'$USERNAME'"]}}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN user.search | jq '.response.data[0].phid'))

# Only FYI: Get list of task IDs edited from feed.query to output in shell:
TASKIDSLISTARRAY=( $(echo '{"filterPHIDs": ['$USERID'], "limit": 100, "view": "text"}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN feed.query | jq '.response[].text' | jq -r 'match("T[0-9]+"; "g").string' | sort -u) )
TASKIDSLISTRAW=$(printf "%s," "${TASKIDSLISTARRAY[@]}")
TASKIDSLIST=$(echo "${TASKIDSLISTRAW//T}")
echo "Tasks to be edited by this script: $PHABURL/maniphest/?ids="$TASKIDSLIST
sleep 10

# Get list of task IDs edited from feed.query; remove duplicates via sort. TODO: Not very sane but kinda works.
TASKIDS=( $(echo '{"filterPHIDs": ['$USERID'], "limit": 100, "view": "text"}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN feed.query | jq '.response[].text' | jq 'match("T[0-9]+"; "g").string' | sort -u) )

for task in "${TASKIDS[@]}"
do
  # TODO: Might replace some of this by custom WM Phab Conduit function?:
  # echo '{"username":"'$USERNAME'"}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN user.transactions

  echo "=== Elements changed by $USERNAME in $task are:"
  # Store partial JSON locally in file:
  echo '{"objectIdentifier":'$task'}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN transaction.search | jq '.response.data[] | select(.authorPHID == '$USERID')' > tmptask

  # "tail -1" to get the oldest action by user (transaction.search does not support "order":"updated"; jq sort only takes an array as input)
  priorityold=`cat tmptask | jq 'select(.type == "priority").fields.old.value?' | tail -1`
  # Extra handling because https://secure.phabricator.com/T12124 ; 
  # TODO: Use proper API here:  echo '{}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.priority.search
  if [ "$priorityold" = 100 ]; then
    priorityold="unbreak"
  elif [ "$priorityold" = 90 ]; then
    priorityold="triage"
  elif [ "$priorityold" = 80 ]; then
    priorityold="high"
  elif [ "$priorityold" = 50 ]; then
    priorityold="normal"
  elif [ "$priorityold" = 25 ]; then
    priorityold="low"
  elif [ "$priorityold" = 10 ]; then
    priorityold="lowest"
  fi
  echo " * priorityold: " $priorityold

  statusold=( $(cat tmptask | jq 'select(.type == "status").fields.old?' | tail -1) )
  echo " * statusold: " $statusold

  ownerold=( $(cat tmptask | jq 'select(.type == "owner").fields.old?' | tail -1) )
  echo " * ownerold: " $ownerold

  titleold=`cat tmptask | jq 'select(.type == "title") | .fields.old?' | tail -1`
  echo " * titleold: " $titleold

  descriptionold=`cat tmptask | jq 'select(.type == "description") | .fields.old?' | tail -1`
  echo " * descriptionold: " $descriptionold

  projectsaddedraw=`cat tmptask | jq 'select(.type == "projects").fields.operations[] | select(.operation == "add").phid?'`
  # TODO: https://unix.stackexchange.com/questions/379181/escape-a-variable-for-use-as-content-of-another-script Hack to work around the " characters in the string:
  projectsaddedC=${projectsaddedraw@E}
  # add commas between values:
  projectsadded=`echo $projectsaddedC | sed 's/ /,/g'`
  echo " * projectsadded: "$projectsadded

  projectsremovedraw=`cat tmptask | jq 'select(.type == "projects").fields.operations[] | select(.operation == "remove").phid?'`
  projectsremovedC=${projectsremovedraw@E}
  projectsremoved=`echo $projectsremovedC | sed 's/ /,/g'`
  echo " * projectsremoved: "$projectsremoved

  subscribersaddedraw=`cat tmptask | jq 'select(.type == "subscribers").fields.operations[] | select(.operation == "add").phid?'`
  subscribersaddedC=${subscribersaddedraw@E}
  subscribersadded=`echo $subscribersaddedC | sed 's/ /,/g'`
  echo " * subscribersadded: "$subscribersadded

  subscribersremovedraw=`cat tmptask | jq 'select(.type == "subscribers").fields.operations[] | select(.operation == "remove").phid?'`
  subscribersremovedC=${subscribersremovedraw@E}
  subscribersremoved=`echo $subscribersremovedC | sed 's/ /,/g'`
  echo " * subscribersremoved: "$subscribersremoved

  echo "=== Going to restore these old values in $task:"

  # Prepare restoring old values:

  if [ -n "$priorityold" ]; then
    revcmdprio='{"type": "priority", "value":"'$priorityold'"},'
  fi
  if [ -n "$statusold" ]; then
    revcmdstatus='{"type": "status", "value":'$statusold'},'
  fi
  if [ -n "$titleold" ]; then
    revcmdtitle='{"type": "title", "value":'$titleold'},'
  fi
  if [ -n "$descriptionold" ]; then
    revcmddesc='{"type": "description", "value":'$descriptionold'},'
  fi
  if [ -n "$ownerold" ]; then
    revcmdowner='{"type": "owner", "value":'$ownerold'},'
  fi
  if [ -n "$projectsremoved" ]; then
    revcmdprojremove='{"type": "projects.add", "value":['$projectsremoved']},'
  fi
  if [ -n "$projectsadded" ]; then
    revcmdprojadd='{"type": "projects.remove", "value":['$projectsadded']},'
  fi
  if [ -n "$subscribersremoved" ]; then
    revcmdsubscribersremove='{"type": "subscribers.add", "value":['$subscribersremoved']},'
  fi
  if [ -n "$subscribersadded" ]; then
    revcmdsubscribersadd='{"type": "subscribers.remove", "value":['$subscribersadded']},'
  fi
  # TODO: Cover Custom Field changes: https://secure.phabricator.com/T13255#244631 and https://secure.phabricator.com/T418#239194
  # Custom field definitions: $PHABURL/config/edit/maniphest.custom-field-definitions/ and $PHABURL/config/edit/maniphest.fields/
  # revcmdcustomduedate='{"type": "custom.deadline.due", "value": "2019-01-06"},' # not sure about date format here. And if Date is even supported in Conduit.

  # Concatenate:
  revcmd="$revcmdccadd$revcmdccremove$revcmdprio$revcmdstatus$revcmdtitle$revcmddesc$revcmdowner$revcmdprojremove$revcmdprojadd$revcmdsubscribersremove$revcmdsubscribersadd"
  # Remove last comma character from string because JSON:
  if [ -n "$revcmd" ]; then
    revcmd=${revcmd::-1}
    echo $revcmd
    echo ""
    # Finally take action on the server:
    echo '{"transactions":['$revcmd'],"objectIdentifier":'$task'}' | /var/www/html/phab/arcanist/bin/arc call-conduit --conduit-uri $PHABURL --conduit-token $CONDUITTOKEN maniphest.edit
  fi

  rm tmptask
  # Reset all values to not apply them to the next task in the list:
  revcmdprio=''
  revcmdstatus=''
  revcmdtitle=''
  revcmddesc=''
  revcmdowner=''
  revcmdprojremove=''
  revcmdprojadd=''
  revcmdsubscribersremove=''
  revcmdsubscribersadd=''
  # TODO: Cover Custom Field changes: https://secure.phabricator.com/T13255#244631 and https://secure.phabricator.com/T418#239194
  # revcmdcustomduedate=''
  sleep 5
done